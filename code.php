<?php 

// [SECTION] Objects as Variables
// Using the procedural approach.
$buildingObj = (object) [
	'name' => 'Caswyn Building',
	'floors' => 8,
	'address' => 'Brgy Sacred Heart, Quezon City, Philippines'
];

// [SECTION] Objects from Classes
class Building{
	// Properties
	public $name;
	public $floors;
	public $address;

	public function __construct($name, $floors, $address){
		$this -> name = $name;
		$this -> floors = $floors;
		$this -> address = $address;
	}

	// Method
	public function printName(){
		return "The name of the building is $this->name";
	}
}
// Instantiate a building class to create a new building object.
$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');

// [SECTION] Inheritance and Polymorphism
/// The "extends" keyword is used to derive a class from another class.
// A derived class/child class has all the properties and methods of the parent class.
class Condominium extends Building{

	// Polymorphism - methods inherited by a derived class that can be overriden to have a behavior diff from the method of base class.
	public function printName(){
		return "The name of the condominium is $this->name";
	}
}
$condominium = new Condominium('Mojo Condo', 5, 'Mabini, Makati City, Philippines');

/// Building 