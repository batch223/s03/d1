<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S03: Classes and Objects</title>
</head>
<body>
	<h2>Objects from Variables</h2>

	<p><?php var_dump($buildingObj);?></p>
	<p><?php echo $buildingObj->name;?></p>

	<h2>Object from Classes</h2>
	<p><?php var_dump($building); ?></p>
	<p><?php echo $building->name;?></p>
	<p><?php echo $building->printName();?></p>

	<h2>Inheritance (Condominium Object)</h2>
	<p><?php var_dump($condominium);?></p>

	<h2>Polymorphism (Changing of printName Behavior)</h2>
	<p><?php echo $condominium->printName();?></p>
</body>
</html>